$('#statusForm').on('submit', function(e) {
    e.preventDefault();
    $.ajax({
        url: '/update',
        type: 'POST',
        data: {
            statusmode: $('#statusModeDropdown').val(),
            msg: $('#msg').val()
        }
    });
});
