var express = require('express');
var bodyParser = require('body-parser');
var fs = require('fs');
var app = express();
var { spawn } = require('child_process');

app.get('/', (req, res) => {
	res.sendFile(__dirname + "/index.html");
});

app.use(bodyParser.urlencoded({extended: true}));

app.post('/update', (req, res) => {
    console.log(req.body);

    fs.readFile('./schedule/status.json', 'utf8', (err, rawStatusJson) => {
        var json = JSON.parse(rawStatusJson);

        if(req.body.statusmode == "Not available"){
            json.status = 'no';
        }else if(req.body.statusmode == "Available for a moment"){
            json.status = 'maybe';
        }else if(req.body.statusmode == "Available"){
            json.status = 'available';
        }

        json.msg = req.body.msg;

        fs.writeFile('./schedule/status.json', JSON.stringify(json), 'utf8', () => {
            var update = spawn('./update.sh');

            update.on('close', (code) => {
                console.log(code);
                res.send("K");
            });
        });
    });
});

app.use(express.static('public'));

app.listen(8000, () => {
	console.log("Started");
});
